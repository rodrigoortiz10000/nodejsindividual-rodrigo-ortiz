Alumno: Rodrigo Ortiz
Matricula: Y18590

Instrucciones para ejecutar el proyecto
Antes que nada, asegurarse de tener todas las librerias necesarias para que el proyecto funcione(nodejs, express, nodemon, etc).

Para abrir y ejecutar el proyecto, primero debes inicializar el servidor con el comando npm run dev. Una vez que el servidor esté en funcionamiento, puedes abrir tu navegador de preferencia y escribir la dirección localhost:3000/bienvenida.

El proyecto consta de tres ventanas:

Ventana de bienvenida: Esta ventana muestra una lista de productos. En la parte inferior de la ventana, hay un botón para crear un nuevo producto.

Ventana de creación de productos: Esta ventana permite crear un nuevo producto. Solo tienes que introducir el nombre y el precio del producto. Al pulsar el botón "Crear producto", el producto se crea automáticamente y se redirige a la ventana de bienvenida.

Ventana de detalle de productos: Esta ventana muestra los detalles de un producto específico. Se puede acceder a ella haciendo clic en la fila "Detalle" del producto que quieras modificar. En esta ventana, puedes cambiar el nombre o el precio del producto, o ambos. También hay un botón para eliminar el producto. Si pulsas el botón "Eliminar", aparecerá una advertencia que debes aceptar si quieres eliminar el producto.

En resumen, el proyecto permite visualizar, crear, modificar y borrar productos.